module World where

import Graphics.Gloss.Data.Point
import Graphics.Gloss.Data.Picture
import Graphics.Gloss.Data.Color as Color

data Room = Room Point
data World = World [Room]

simulateWorld w = w

renderWorld :: World -> Picture
renderWorld (World rooms) = translate 0 (-200.0) $
     pictures [renderRooms rooms, renderGround]

renderRooms :: [Room] -> Picture
renderRooms = pictures . map renderRoom

roomWidth = 100.0
roomHeight = 40.0

renderRoom :: Room -> Picture
renderRoom (Room pos@(x,y) ) = color Color.orange $
    translate x (y + roomHeight * 0.5) $ rectangleSolid roomWidth roomHeight

groundHeight = 10000.0
renderGround :: Picture
renderGround = color Color.green $
    translate 0 (-(groundHeight * 0.5)) $ rectangleSolid 1000.0 groundHeight
