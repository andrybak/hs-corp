import Prelude (IO, id, ($), Float, (+), (-), (*), undefined)
import Data.Maybe
import Control.Monad

import System.Environment
import System.Exit

import Graphics.Gloss.Interface.IO.Game as G
import Graphics.Gloss.Data.Picture
import Graphics.Gloss.Data.Color as Color

import Menu
import Splash
import Debug
import World


data GameState = GameState ScreenState (Maybe World)
data ScreenState = Splash SplashType | Playing | InMenu Menu

displayMode = InWindow "Haskell Corp." (800, 600) (450, 150)
simulationRate = 1000

initialState :: GameState
initialState = GameState (InMenu $ createMenu JustStarted) Nothing

paintGame :: GameState -> IO Picture
paintGame (GameState s w) = paintScreen s w

paintScreen :: ScreenState -> Maybe World -> IO Picture
paintScreen (InMenu m) _ = return $ renderMenu m
paintScreen Playing (Just w) = return $ renderWorld w
paintScreen Playing Nothing = undefined
paintScreen _ _ = return $ pictures [notImplemented, debugPic]

handleInputs :: Event -> GameState -> IO GameState
handleInputs e (GameState (InMenu m) x) = return $ GameState (InMenu $ handleMenuInputs e m) x
handleInputs _ gs = return gs

play w = GameState Playing (Just w)
newGame = play $ World $ [ Room (0.0, 0.0) ]

simulate :: Float -> GameState -> IO GameState
simulate _ (GameState (InMenu (Selected Quit)) _) = exitWith ExitSuccess
simulate _ (GameState (InMenu (Selected NewGame)) _) = return $ newGame
simulate _ (GameState Playing (Just w)) = return $ play $ simulateWorld w
simulate _ (GameState Playing Nothing) = undefined
simulate _ x = return x -- do nothing

main :: IO ()
main = playIO displayMode Color.black simulationRate initialState
    paintGame
    handleInputs
    simulate
